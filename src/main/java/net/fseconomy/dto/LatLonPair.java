package net.fseconomy.dto;

import java.io.Serializable;

public class LatLonPair implements Serializable
{
    public String f;
    public String t;
    public boolean l;
    public double a;
    public double b;
    public double c;
    public double d;

    public LatLonPair(String from, String to, boolean locked, double latitude, double longitude, double latitude2, double longitude2)
    {
        f = from;
        t = to;
        l = locked;
        a = latitude;
        b = longitude;
        c = latitude2;
        d = longitude2;
    }
}
