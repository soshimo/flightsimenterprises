package net.fseconomy.dto;

import java.sql.Timestamp;

//New class to hold a single data point for an fbo lottery
public class FboLottery
{
    public int id;
    public int status;
    public Timestamp start;
    public Timestamp end;
    public Timestamp closed;
    public int fboid;
    public String fboicao;
    public int ticketAmount;
    public int amountDue;
    public int ticketsSold;

    public FboLottery(int id, int status, Timestamp start, Timestamp end, Timestamp closed, int fboid, String fboicao, int ticketAmount, int amountDue, int ticketsSold)
    {
        this.id = id;
        this.status = status;
        this.start = start;
        this.end = end;
        this.closed = closed;
        this.fboid = fboid;
        this.fboicao = fboicao;
        this.ticketAmount = ticketAmount;
        this.amountDue = amountDue;
        this.ticketsSold = ticketsSold;
    }
}


