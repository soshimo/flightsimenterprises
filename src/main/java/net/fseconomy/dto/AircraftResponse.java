package net.fseconomy.dto;

import net.fseconomy.beans.AircraftBean;

import java.sql.SQLException;

/**
 * Created by smobl on 1/28/2019.
 */
public class AircraftResponse
{
    public static enum AccessStatus
    {
        NOTFOUND_NOACCESS,
        SUCCESS,
        ERROR
    }

    public AccessStatus status;
    public String cashBalance;
    public AircraftBean aircraft;

    public AircraftResponse(AccessStatus status, AircraftBean aircraft, String cashBalance)
    {
        this.status = status;
        this.cashBalance = cashBalance;
        this.aircraft = aircraft;
    }
}
