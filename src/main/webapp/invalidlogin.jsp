<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import="net.fseconomy.util.Helpers"
%>

<%
%>
<jsp:include flush="true" page="/head.jsp" />
</head>
<body>

<jsp:include flush="true" page="top.jsp" />
<jsp:include flush="true" page="menu.jsp" />

<div id="wrapper">
<div class="content">
<%
	String message = Helpers.getSessionMessage(request);
	if (message != null)
    {
%>
	<div class="message"><%= message %></div>
<%
	}
%>

	<h2>Login incorrect</h2>
	<p>
	    If you don't remember your password you can request a new password here:
	</p>
	<div class="form" style="width: 400px">
        <form method="post" action="userctl">
            Username<br/>
            <input name="user" type="text" class="textarea" size="10" /><br/>
            Email<br/>
            <input name="email" type="text" class="textarea" size="40" /><br/><br/>
            <input type="submit" class="button" value="Request password" />
            <input type="hidden" name="event" value="password"/>
            <input type="hidden" name="return" value="invalidlogin.jsp"/>
        </form>
	</div>

</div>
</div>
</body>
</html>
